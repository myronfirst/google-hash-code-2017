/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ghashcode;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.PriorityQueue;
import java.util.Scanner;

/**
 *
 * @author a
 */
public class main {
    static Video[] videos;
    static Endpoint[] endpoints;
    static CacheServer[] servers;
    static PriorityQueue<CacheServer> visitSequence;
    static String fName;
    
    public static void main(String[] args) throws IOException
    {
        readInput();
        visitSequence = new PriorityQueue<>();
        PriorityQueue<CacheServer> tmp = new PriorityQueue<>();
        prioritize();
        int numOfVids = videos.length;
        int usedServers = 0;
        while(!visitSequence.isEmpty())
        {
            CacheServer current = visitSequence.remove();
            tmp.add(current);
            current.assignVideos(videos);
            if(!current.serverVids.isEmpty())
                usedServers++;
        }
        while(!tmp.isEmpty())
        {
            CacheServer current = tmp.remove();
            current.optimizeLeftOvers(videos);
        }
        PrintWriter output = new PrintWriter(new FileWriter(fName.split("\\.")[0]+".out"));
        output.write(usedServers+"\n");
        for(CacheServer c: servers)
        {
            if(!c.serverVids.isEmpty())
            {
                output.write(c.id+" ");
                for(Video v: c.serverVids)
                {
                    output.write(v.id+" ");
                }
                output.write("\n");
            }
        }
        output.close();
    }
    public static void readInput()
    {
        System.out.println("Give input file path.");
        Scanner in = new Scanner(System.in);
        fName = in.nextLine();
        try{
            Scanner input = new Scanner( new FileReader(fName) );
            int numOfVideos = input.nextInt();//get number of videos
            videos = new Video[numOfVideos];
            int numOfEnds = input.nextInt();//get number of endpoints
            endpoints = new Endpoint[numOfEnds];
            int numOfReqs = input.nextInt();
            int numOfServers = input.nextInt();
            servers = new CacheServer[numOfServers];
 
            int capacity = input.nextInt();
            for(int i=0; i<numOfServers; i++)
            {
                servers[i] = new CacheServer(i, capacity);
            }
            for(int i=0; i<numOfVideos; i++)
            {
                videos[i] = new Video(i, input.nextInt() );//initialize videos
            }
            for(int i=0; i<numOfEnds; i++)
            {
                //initialize endpoints
                int cLat = input.nextInt();
                int K = input.nextInt();
                endpoints[i] = new Endpoint(i, cLat, K );
                for(int j=0; j<K; j++)//initialize this endpoint's connections
                {
                    int currentServer = input.nextInt();                
                    Connection c = new Connection(endpoints[i], 
                            servers[currentServer], input.nextInt());
                    endpoints[i].connections[j] = c;
                    servers[currentServer].addConnection(c);
                    //if(currentServer == 0) System.out.println("Server 0 connected to end "+i);
                }
            }
            for(int i=0; i<numOfReqs; i++)//read the requests
            {
                int reqVid = input.nextInt();//requested video
                int endpId = input.nextInt();//endpoint that made the request
                int currentVidReqs = input.nextInt();//size of request
                endpoints[endpId].addRequest(videos[reqVid], currentVidReqs);
                //if (endpId == 5 || endpId ==1 || endpId == 0) 
                       // System.out.println("Endpoint "+endpId+" wants vid "+reqVid+" times "+currentVidReqs);
            }
            input.close();
        }
        catch(FileNotFoundException e)
        {
        }
    }
    
    public static void prioritize()
    {
        for (Endpoint e: endpoints)//assign traffics to all connections of all endpoints
        {
            e.assignTraffic();
        }
        for(CacheServer c: servers)//assign server priorities
        {
            c.assignPriority();
            visitSequence.add(c);
        }
    }
}
