/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ghashcode;

/**
 *
 * @author a
 */
public class Connection {
    Endpoint connectedEnd;
    CacheServer connectedServer;
    int latency;
    double traffic;
    
    public Connection(Endpoint e, CacheServer c, int lat)
    {
        connectedEnd = e;
        connectedServer = c;
        latency = lat;
    }
}
