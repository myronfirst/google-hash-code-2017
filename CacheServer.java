/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ghashcode;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 *
 * @author a
 */
class SecComparator implements Comparator<ServerVideo>{
    public int compare(ServerVideo v1, ServerVideo v2)
    {
        if(v1.timeSaved/(double)v1.vid.size == v2.timeSaved/(double)v2.vid.size)
            return 0;
        return (v1.timeSaved/(double)v1.vid.size-v2.timeSaved/(double)v2.vid.size>0)?(-1):(1);
    }
}
public class CacheServer implements Comparable<CacheServer> {
    int id;
    int capacity;
    int totalVidSize;
    double priority;
    ArrayList<Connection> connections;
    ArrayList<Video> serverVids;
    public CacheServer(int id, int capacity)
    {
        this.id = id;
        this.capacity = capacity;
        connections = new ArrayList<>();
        serverVids = new ArrayList<>();
        priority = 0;
        totalVidSize = 0;
    }
    public void addConnection(Connection c)
    {
        connections.add(c);
    }
    public void assignPriority()
    {
        for(Connection c: connections)
        {
            priority += c.traffic/c.latency;
        }
        priority /= connections.size();
    }
    public int compareTo(CacheServer c)
    {
        if(c.priority == priority) return 0;
        return ( (priority-c.priority)<0 )?(1):(-1);
    }
    public void assignVideos(Video[] allVids)
    {
        PriorityQueue<ServerVideo> vidSequence = new PriorityQueue<>();
        int totalVids = allVids.length;
        for(int i=0; i<totalVids; i++)//initialize the priority queue of this server's videos
        {
            int realRequests=0;
            boolean atLeastOne = false;
            for(Connection c: connections)
            {
                Pair p = c.connectedEnd.requests.get(allVids[i]);
                if(p != null)
                {
                    atLeastOne = true;
                    if(p.alreadyServed == false)
                        realRequests += p.numOfRequests;
                }
            }
            if(atLeastOne == true && realRequests != 0)
            {
                vidSequence.add(new ServerVideo(allVids[i], realRequests));     
            }
        }
        //now the priority queue is ready
        while(!vidSequence.isEmpty() && totalVidSize<capacity)
        {
            ServerVideo v = vidSequence.remove();
            //System.out.println("I like video"+v.vid.id+" and I am No. "+id+" hits: "+v.metric);
            if(v.vid.size + totalVidSize <= capacity)
            {
                serverVids.add(v.vid);
                totalVidSize += v.vid.size;
                for(Connection c: connections)
                {
                    Pair current = c.connectedEnd.requests.get(v.vid);
                    if(current!=null)
                    {
                        current.alreadyServed = true;
                        if(current.bestForVid == null || c.latency < current.bestForVid.latency)
                            current.bestForVid = c;
                    }
                }
            }
        }
    }
    
    public void optimizeLeftOvers(Video[] allVids)
    {
        PriorityQueue<ServerVideo> secondarySequence = new PriorityQueue<>(new SecComparator());
        int totalVids = allVids.length;
        for(int i=0; i<totalVids; i++)//initialize the priority queue of this server's videos
        {
            int timeSaved=0;
            if(!serverVids.contains(allVids[i]))
            {
                for(Connection c: connections)
                {
                    Pair p = c.connectedEnd.requests.get(allVids[i]);
                    if(p!=null)
                    {
                        if(p.alreadyServed == true)
                            timeSaved += (c.latency - p.bestForVid.latency)*p.numOfRequests;         
                        else
                            timeSaved += (c.connectedEnd.centerLatency - c.latency)*p.numOfRequests;
                    }
                }
                ServerVideo tmp = new ServerVideo(allVids[i], 0);
                tmp.timeSaved = timeSaved;
                secondarySequence.add(tmp);
            }
        }
        while(!secondarySequence.isEmpty() && totalVidSize<capacity)
        {
            ServerVideo v = secondarySequence.remove();
            //System.out.println(v.timeSaved);
            if(v.vid.size + totalVidSize <= capacity)
            {
                serverVids.add(v.vid);
                totalVidSize += v.vid.size;
                for(Connection c: connections)
                {
                    Pair current = c.connectedEnd.requests.get(v.vid);
                    if(current!=null)
                    {
                        if(current.alreadyServed == false)
                        {
                            current.alreadyServed = true;
                            current.bestForVid = c;
                        }
                        else if(current.bestForVid.latency>c.latency)
                            current.bestForVid = c;                           
                    }
                
                }               
            }
        }        
    }
}
