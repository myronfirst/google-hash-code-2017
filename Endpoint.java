/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ghashcode;

import java.util.HashMap;

/**
 *
 * @author a
 */
public class Endpoint {
    int id;
    int centerLatency;//latency of connection to data center
    int numOfConnections;
    int totalReqs;
    Connection[] connections;
    HashMap<Video, Pair> requests;//maps video ids to number of requests
    
    public Endpoint(int id, int cLat, int numCons)
    {
        centerLatency = cLat;
        this.id = id;
        numOfConnections = numCons;
        //System.out.println(id+" "+cLat+" "+numOfConnections);
        connections = new Connection[numCons];
        requests = new HashMap<>();
        totalReqs = 0;
    }
    
    public void addRequest(Video vid, int howMany)
    {
        Pair old = requests.get(vid);
        if(old == null) //first request of this vid
            requests.put(vid, new Pair(howMany, false) );
        else
            requests.put(vid, new Pair(howMany+old.numOfRequests, false));
        totalReqs += howMany;
    }
    
    public void assignTraffic()
    {
        if(connections.length == 0) return;
        int traf = totalReqs/connections.length;
        for(Connection c: connections)
        {
            c.traffic = traf;
        }
    }
}
