/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ghashcode;

/**
 *
 * @author a
 */
public class ServerVideo implements Comparable<ServerVideo>{
    Video vid;
    double metric;
    double timeSaved;
    public ServerVideo(Video v, int requests)
    {
        vid = v;
        metric = ((double)requests)/vid.size;
    }
    public int compareTo(ServerVideo v)
    {
        if(metric == v.metric) return 0;
        return (metric<v.metric)?(1):(-1);
    }
}
